# upgrade_app

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

![Alt text](/lib/screenshots/welcome.png?raw=true "the welcome screen")
![Alt text](/lib/screenshots/Login.png?raw=true "the login screen")
![Alt text](/lib/screenshots/SignUp.png?raw=true "the signup screen")
![Alt text](/lib/screenshots/Verification.png?raw=true "the email verification screen")
![Alt text](/lib/screenshots/changePassword.png?raw=true "the change password screen")
![Alt text](/lib/screenshots/resetPassword.png?raw=true "the reset password screen")

