class Validators {
  static String textFieldValidator(String value) {
    if (value.isEmpty) {
      return "This field should not be empty";
    }

    return null;
  }

  static String passwordValidator(String value) {
    if (value.isEmpty) {
      return "This field should not be empty";
    } else if (value.length < 8) {
      return "Password should be atleast 8 characters";
    }
    return null;
  }

  static String emailValidator(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(email))
      return 'Enter Valid Email';
    else
      return null;
  }
}
