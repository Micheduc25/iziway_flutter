import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final String label;
  final String assetName;
  LoginButton({this.label, this.assetName});
  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      borderSide: BorderSide(color: Colors.grey[200]),
      padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
              width: 22, height: 22, child: Image.asset("assets/$assetName")),
          Padding(
            padding: EdgeInsets.only(left: 10),
          ),
          Text(
            this.label,
            style: TextStyle(fontSize: 17),
          )
        ],
      ),
      onPressed: () {},
    );
  }
}
