import 'package:flutter/material.dart';
import 'package:upgrade_app/screens/changePassword.dart';
import 'package:upgrade_app/screens/home.dart';
import 'package:upgrade_app/screens/login.dart';
import 'package:upgrade_app/screens/resetPassword.dart';
import 'package:upgrade_app/screens/signUp.dart';
import 'package:upgrade_app/screens/verification.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        'home': (context) => HomeScreen(),
        'reset': (context) => ResetPassScreen(),
        'login': (context) => LoginScreen(),
        'signup': (context) => SignUpScreen(),
        'change': (context) => ChangePassScreen()
      },
      home: HomeScreen(),
    );
  }
}
