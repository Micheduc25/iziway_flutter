import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:upgrade_app/components/loginBut.dart';
import 'package:upgrade_app/utils/colors.dart';
import 'package:upgrade_app/utils/validators.dart';

class VerificationScreen extends StatefulWidget {
  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  GlobalKey<FormState> _formkey;
  bool _autoValidate;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _autoValidate = false;
    _formkey = GlobalKey<FormState>();
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Verification screen",
      home: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              //the heading here
              child: Padding(
                padding:
                    EdgeInsets.only(top: 40, left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Verification",
                              style: TextStyle(
                                  fontSize: 35.0, fontWeight: FontWeight.bold)),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(
                              "we will send you a Pin to continue your account",
                              style: TextStyle(fontSize: 15.0))
                        ],
                      ),
                    ),

                    //the body of the sign up
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: EdgeInsets.all(25),
                        child: Form(
                          key: _formkey,
                          autovalidate: _autoValidate,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "Pin",
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              //verification fields
                              Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: PinCodeTextField(
                                    maxLength: 6,
                                    pinBoxWidth: 42,
                                    pinBoxHeight: 42,
                                    defaultBorderColor: MyColors.orange,
                                    hasTextBorderColor: MyColors.orange,
                                    onDone: (value) {
                                      print(value);
                                    },
                                  )),

                              //password

                              Padding(
                                padding: EdgeInsets.only(top: 30),
                              ),

                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: MyColors.orange,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15),
                                    child: Text(
                                      "Submit",
                                    ),
                                    onPressed: () => submitForm(context)),
                              ),

                              Padding(
                                padding: EdgeInsets.only(bottom: 20),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  submitForm(BuildContext context) {
    if (_formkey.currentState.validate()) {
      print("Signup successful");
      setState(() {});
    } else {
      print("an error occured");
      _autoValidate = true;
    }
  }
}
