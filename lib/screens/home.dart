import 'package:flutter/material.dart';
import 'package:upgrade_app/utils/colors.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return MaterialApp(
      title: "home screen",
      home: Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Image.asset(
                      'assets/logo.webp',
                      width: size.width * 0.4,
                      height: size.height * 0.3,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        "IZIWAY",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 30,
                            fontFamily: "comic sans ms",
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    SizedBox(
                        width: double.infinity,
                        child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: MyColors.orange,
                            textColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                            child: Text(
                              "Sign Up",
                            ),
                            onPressed: () {
                              Navigator.of(context).pushNamed('signup');
                            })),
                    Padding(
                      padding: EdgeInsets.only(top: 30),
                    ),
                    SizedBox(
                        width: double.infinity,
                        child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            color: MyColors.orange,
                            textColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                            child: Text(
                              "Login",
                            ),
                            onPressed: () {
                              Navigator.of(context).pushNamed('login');
                            })),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
