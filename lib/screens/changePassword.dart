import 'package:flutter/material.dart';
import 'package:upgrade_app/components/loginBut.dart';
import 'package:upgrade_app/utils/colors.dart';
import 'package:upgrade_app/utils/validators.dart';

class ChangePassScreen extends StatefulWidget {
  @override
  _ChangePassScreenState createState() => _ChangePassScreenState();
}

class _ChangePassScreenState extends State<ChangePassScreen> {
  GlobalKey<FormState> _formkey;
  bool _showPassword1;
  bool _showPassword2;
  bool _autoValidate;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _showPassword1 = false;
    _showPassword2 = false;
    _autoValidate = false;
    _formkey = GlobalKey<FormState>();
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login screen",
      home: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              //the heading here
              child: Padding(
                padding:
                    EdgeInsets.only(top: 40, left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Change Password",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 35.0, fontWeight: FontWeight.bold)),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text("Sign in to continue",
                              style: TextStyle(fontSize: 15.0))
                        ],
                      ),
                    ),

                    //the body of the sign up
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: EdgeInsets.all(25),
                        child: Form(
                          key: _formkey,
                          autovalidate: _autoValidate,
                          child: Column(
                            children: <Widget>[
                              //old password
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    decoration: InputDecoration(
                                        labelText: "Old password",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        ))),
                                    validator: (value) {
                                      return Validators.passwordValidator(
                                          value);
                                    }),
                              ),

                              //password
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    obscureText: !_showPassword1,
                                    decoration: InputDecoration(
                                        labelText: "New Password",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        )),
                                        suffixIcon: IconButton(
                                          icon: Icon(!_showPassword1
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                          onPressed: () {
                                            setState(() {
                                              _showPassword1 = !_showPassword1;
                                            });
                                          },
                                        )),
                                    validator: (value) {
                                      return Validators.passwordValidator(
                                          value);
                                    }),
                              ),

                              //password
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    obscureText: !_showPassword2,
                                    decoration: InputDecoration(
                                        labelText: "Confirm New Password",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        )),
                                        suffixIcon: IconButton(
                                          icon: Icon(!_showPassword2
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                          onPressed: () {
                                            setState(() {
                                              _showPassword2 = !_showPassword2;
                                            });
                                          },
                                        )),
                                    validator: (value) {
                                      return Validators.passwordValidator(
                                          value);
                                    }),
                              ),

                              Padding(
                                padding: EdgeInsets.only(top: 20),
                              ),

                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: MyColors.orange,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15),
                                    child: Text(
                                      "Update Password",
                                    ),
                                    onPressed: () => submitForm(context)),
                              ),

                              Padding(
                                padding: EdgeInsets.only(bottom: 30),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  submitForm(BuildContext context) {
    if (_formkey.currentState.validate()) {
      print("Signup successful");
      setState(() {});
    } else {
      print("an error occured");
      _autoValidate = true;
    }
  }
}
