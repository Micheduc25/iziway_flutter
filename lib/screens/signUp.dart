import 'package:flutter/material.dart';
import 'package:upgrade_app/utils/colors.dart';
import 'package:upgrade_app/utils/validators.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  GlobalKey<FormState> _formkey;
  bool _showPassword;
  bool _autoValidate;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _showPassword = false;
    _autoValidate = false;
    _formkey = GlobalKey<FormState>();
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Sign up screen",
      home: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              //the heading here
              child: Padding(
                padding:
                    EdgeInsets.only(top: 40, left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Welcome",
                              style: TextStyle(
                                  fontSize: 35.0, fontWeight: FontWeight.bold)),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text("Sign up to continue",
                              style: TextStyle(fontSize: 15.0))
                        ],
                      ),
                    ),

                    //the body of the sign up
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: EdgeInsets.all(25),
                        child: Form(
                          key: _formkey,
                          autovalidate: _autoValidate,
                          child: Column(
                            children: <Widget>[
                              //name
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Name",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        ))),
                                    validator: (value) {
                                      return Validators.textFieldValidator(
                                          value);
                                    }),
                              ),

                              //email
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    decoration: InputDecoration(
                                        labelText: "Email",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        ))),
                                    validator: (value) {
                                      return Validators.emailValidator(value);
                                    }),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        labelText: "Phone Number",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        ))),
                                    validator: (value) {
                                      return Validators.textFieldValidator(
                                          value);
                                    }),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    obscureText: !_showPassword,
                                    decoration: InputDecoration(
                                        labelText: "Password",
                                        labelStyle: TextStyle(fontSize: 16),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          color: MyColors.orange,
                                        )),
                                        suffixIcon: IconButton(
                                          icon: Icon(!_showPassword
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                          onPressed: () {
                                            setState(() {
                                              _showPassword = !_showPassword;
                                            });
                                          },
                                        )),
                                    validator: (value) {
                                      return Validators.passwordValidator(
                                          value);
                                    }),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    color: MyColors.orange,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15),
                                    child: Text(
                                      "Sign Up",
                                    ),
                                    onPressed: () => submitForm(context)),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Already have an account ?"),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 3),
                                  ),
                                  FlatButton(
                                      textColor: MyColors.oxred,
                                      child: Text("Sign in"),
                                      onPressed: () {})
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  submitForm(BuildContext context) {
    if (_formkey.currentState.validate()) {
      print("Signup successful");
      setState(() {});
    } else {
      print("an error occured");
      _autoValidate = true;
    }
  }
}
